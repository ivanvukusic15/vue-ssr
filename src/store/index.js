import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export function createStore() {
  return new Vuex.Store({
    state() {
      return {
        weather: {}
      };
    },
    mutations: {
      setWeatherResults(state, payload) {
        state.weather = payload;
      }
    },
    actions: {},
    modules: {}
  });
}
